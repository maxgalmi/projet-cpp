#ifndef CARTEUNO_H
#define CARTEUNO_H

#include "Carte.h"

class PileDeCartes;
class CarteUno : public Carte
{
    public:

        CarteUno(int,std::string,int=0,bool=true);
        virtual void affiche()const;
        virtual ~CarteUno();
        static int recherche(std::string,PileDeCartes*);
        CarteUno(const CarteUno& other);
    protected:
    private:
};

#endif // CARTEUNO_H
