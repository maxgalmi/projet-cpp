#include "Bataille.h"
#include "PileDeCartes.h"

using namespace std;

Bataille::Bataille(int j, int o):JeudeCartes(j,o)
{
    if(j+o!=2) throw string("2 joueurs uniquement");
    initialisation();


}

void Bataille::initialisation(){
    for(int i = 1;i<14;i++){
            pioche.ajouter(new CarteClassique(i,"Ca"));
            pioche.ajouter(new CarteClassique(i,"Co"));
            pioche.ajouter(new CarteClassique(i,"P"));
            pioche.ajouter(new CarteClassique(i,"T"));
        }

  while(pioche.getSize()>0) {
        pioche.piocher(&pioche1);
        pioche.piocher(&pioche2);
  }
  pioche1.melange();
  pioche2.melange();
}




void Bataille::affiche_regles(){


    cout<<"On distribue l'ensemble d'un (ou plusieurs) jeu de cartes (52 ou 32) aux joueurs, qui n'en prennent pas connaissance. À chaque tour, chaque joueur retourne la carte du haut de sa main (ou son tas). Celui qui a la carte de la plus haute valeur — selon la hiérarchie du bridge : As, Roi, Dame, Valet, dix… jusqu'au deux — fait la levée, qu'il place sous son tas."<<endl;
    cout<< "En cas d'égalité de valeurs — cas appelé bataille — les joueurs commencent par placer une première carte face cachée puis une seconde carte face visible pour décider qui fera la levée. En cas de nouvelle égalité, la procédure est répétée. La partie se termine et le gagnant est déterminé lorsque ce dernier a en sa possession toutes les cartes du jeu."<<endl;



}


int Bataille::gagne_manche(){
int n = joueurs[0].main.getSize(); // = joueurs[1].main.nbcartes
    if(n!=0){
        if (joueurs[0].main.getValeur(n-1) == joueurs[1].main.getValeur(n-1)) return 0;
        else if (joueurs[0].main.getValeur(n-1)==1){
            return 1;
        }else if (joueurs[1].main.getValeur(n-1)==1){
            return 2;
        }else if (joueurs[0].main.getValeur(n-1)>joueurs[1].main.getValeur(n-1)) return 1;
        else return 2;
    }else return -1;
}



int Bataille::gagne(){
    if (pioche1.getSize() == 0) return 2;
    else if (pioche2.getSize() == 0) return 1;
    else return 0;
}

void Bataille::affiche(){
    if (joueurs[1].humain) cout << "Joueur " << (joueurs[1].id) << " :     " ;
    else cout << "Ordinateur " << (joueurs[1].id) << " :  " ;
    pioche2.affiche(); joueurs[1].main.affiche(); cout<<endl;
    if (joueurs[0].humain) cout << "Joueur " << (joueurs[0].id) << " :      " ;
    else cout << "Ordinateur " << (joueurs[0].id) << " :  " ;
    pioche1.affiche(); joueurs[0].main.affiche(); cout<<endl;
}

void Bataille::ajoutergagnant(int n){

    if (n==1){
        if (joueurs[0].humain) cout << "Le joueur " << (joueurs[0].id) << " gagne la manche" << endl;
        else cout << "L'ordinateur " << (joueurs[0].id) << " gagne la manche" << endl;
        joueurs[0].main.deplacertout(&pioche1);
        joueurs[1].main.deplacertout(&pioche1);
    }else if(n==2){
        if (joueurs[1].humain) cout << "Le joueur " << (joueurs[1].id) << " gagne la manche" << endl;
        else cout << "L'ordinateur " << (joueurs[1].id) << " gagne la manche" << endl;
        joueurs[0].main.deplacertout(&pioche2);
        joueurs[1].main.deplacertout(&pioche2);
    }
}

void Bataille::tour_termine(bool t){
         cout << "Appuyez sur Entrée pour continuer" << endl;
        getchar();
}

void Bataille::piocher(int n){
    for (int i = 0;i<n;i++){
        pioche1.piocher(&joueurs[0].main);
        pioche2.piocher(&joueurs[1].main);
    }
}

void Bataille::jouer(){

    affiche();
    while(gagne()==0){
        tour_termine();
        piocher();
        affiche();
        while(gagne_manche()==0){
            if (pioche1.getSize()<2||pioche2.getSize()<2){
                cout << "Pas assez de cartes pour une bataille."<<endl;
                if (pioche1.getSize()<pioche2.getSize()){
                    affiche_gagnant(1);
                }else if (pioche1.getSize()>pioche2.getSize()){
                    affiche_gagnant(0);
                }else affiche_gagnant(-1);
                return;
            }else{
                cout<<"Bataille"<<endl;
                cout << "Appuyer sur Entrée pour continuer" <<endl;
               getchar();
                 pioche1.setVisible(0,false);
                 pioche2.setVisible(0,false);
                 piocher(2);
                 affiche();
            }

        }ajoutergagnant(gagne_manche());
        pioche1.setVisible();
        pioche2.setVisible();
    }affiche();affiche_gagnant(gagne()-1);

}

Bataille::~Bataille()
{
    //dtor
}
