#include "PileDeCartes.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;


PileDeCartes::PileDeCartes(string t): type(t)
{
}


void PileDeCartes::ajouter(Carte* c){
    tab.push_back(c);
}

void PileDeCartes::supprimer(){
   // delete tab[tab.size()-1];
    tab.pop_back();
}

void PileDeCartes::vider(){
     while(tab.size()!=0){
     //  delete tab[tab.size()-1];
        tab.pop_back();;
        }
}


void PileDeCartes::melange(){

   if(tab.size()>1){
    for (int i=0;i<1000;i++){
        int n = rand() % tab.size();
        int m = rand() % tab.size();
        swap(tab[n],tab[m]);
    }
   }
}


int PileDeCartes::getSize() const{
    return tab.size();
}




int PileDeCartes::getValeur(int n) const{
    if(n>-1&&n<tab.size())
        return tab[n]->getValeur();
}

int PileDeCartes::getGain(int n) const{
    if(n>-1&&n<tab.size())
        return tab[n]->getGain();
}

string PileDeCartes::getCouleur(int n) const{
    if(n>-1&&n<tab.size())
        return tab[n]->getCouleur();
}

void PileDeCartes::placer(int n,PileDeCartes* p){
    p->ajouter(tab[n]);
    for (int i=n;i<(tab.size()-1);i++){tab[i]=tab[i+1];}
    tab.pop_back();
}

void PileDeCartes::piocher(PileDeCartes* p,int n){
   for (int i=0;i<n;i++)
    placer(0,p);
}

void PileDeCartes::deplacertout(PileDeCartes* p){
    while(tab.size()>0){
    piocher(p);}
    p->melange();
}

void PileDeCartes::setVisible(int n, bool b){
    if(n>-1&&n<tab.size())
        tab[n]->setVisible(b);
}

void PileDeCartes::setHidden(){
   for (int i=0;i<tab.size();i++){
            tab[i]->setVisible(false);
        }
}


void PileDeCartes::setVisible(){
   for (int i=0;i<tab.size();i++){
            tab[i]->setVisible(true);
        }
}


void PileDeCartes::affiche()const{
    if (type == "pioche"){cout<<"["<< tab.size()<<"] ";}
    else if (type=="pile"){tab[tab.size()-1]->affiche();}
    else if (type=="main"||type=="tapis"){
        for (int i=0;i<tab.size();i++){
            tab[i]->affiche();
        }
    }
}



