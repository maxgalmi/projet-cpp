#ifndef JOUEUR_H
#define JOUEUR_H
#include "PileDeCartes.h"

class Joueur
{
    public:
        int id;
        bool humain;
        int score;
        PileDeCartes main= PileDeCartes("main");
        PileDeCartes plis= PileDeCartes("plis");
        Joueur(int,bool);
        void affiche(bool=true);

};

#endif // JOUEUR_H
