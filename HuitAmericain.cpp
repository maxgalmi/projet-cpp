#include "HuitAmericain.h"
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
using namespace std;

HuitAmericain::HuitAmericain(int j, int o,int p):JeudeCartes(j,o)
{   if(j+o<2||j+o>5) throw string("2 à 5 joueurs recquis");
    points = p;
    initialisation();


}

void HuitAmericain::initialisation(){
    melange();
    sens = "horaire";
    joueurcourant=0;
    pioche.vider();
    pile.vider();
    for(int i=0;i<joueurs.size();i++){
        while(joueurs[i].main.getSize()!=0)
            joueurs[i].main.vider();
    }


    for(int i = 1;i<14;i++){
        int g;
        if(i==12||i==13)
            g= 10;
        else if (i==10||i==11||i==2||i==7)
            g=20;
        else if (i==1||i==8)
            g=50;
        else g=i;
            pioche.ajouter(new CarteClassique(i,"Ca",g));
            pioche.ajouter(new CarteClassique(i,"Co",g));
            pioche.ajouter(new CarteClassique(i,"P",g));
            pioche.ajouter(new CarteClassique(i,"T",g));
        }
        pioche.melange();
        pioche.piocher(&pile);
        couleurcourante = pile.getCouleur(pile.getSize()-1);
        for (int i=0;i<joueurs.size();i++){
            pioche.piocher(&joueurs[i].main,8);
        }

}


void HuitAmericain::calcul_score(int gagnant){
int v;int s;
    for (int i=0;i<joueurs.size();i++){
        for (int j=0;j<joueurs[i].main.getSize();j++){
            v = joueurs[i].main.getGain(j);
            joueurs[gagnant].score +=v;
        }

    }
}

int HuitAmericain::gagne_manche(){
    for(int i=0;i<joueurs.size();i++){
        if (joueurs[i].main.getSize() == 0) return i;
    }
    return -1;
}

int HuitAmericain::gagne(){
    for(int i=0;i<joueurs.size();i++){
        if (joueurs[i].score >= points) return i;
    }return -1;
}


vector<int> HuitAmericain::coups_possibles(){
    int v = pile.getValeur(pile.getSize()-1);
    vector<int> coups;
    for (int i = 0; i<joueurs[joueurcourant].main.getSize();i++){
        if(joueurs[joueurcourant].main.getValeur(i)==8)
            coups.push_back(i);
        else{
            if(v==joueurs[joueurcourant].main.getValeur(i))
                coups.push_back(i);
            else if (joueurs[joueurcourant].main.getCouleur(i)==couleurcourante)
                coups.push_back(i);
        }
    }
    return coups;
}

vector<int> HuitAmericain::coups_possibles_contre(){
    vector<int> coups;
    for (int i = 0; i<joueurs[joueurcourant].main.getSize();i++){
        if(joueurs[joueurcourant].main.getValeur(i)==8)
            coups.push_back(i);
        else if(joueurs[joueurcourant].main.getValeur(i)==1)
                coups.push_back(i);
    }

    return coups;
}

void HuitAmericain::piocher(int n){
    if (pioche.getSize()<n){
        pile.deplacertout(&pioche);
        pioche.piocher(&pile);
        if(pioche.getSize()==0){
            cout << "Pioche vide. Piochage annulé." << endl;
        }else{
            if (pioche.getSize()>=n){
                pioche.piocher(&joueurs[joueurcourant].main,n);
            }else{
                cout << "Pas assez de cartes dans la pioche, piochage de " << pile.getSize() << " cartes"<<endl;
                pioche.piocher(&joueurs[joueurcourant].main,pile.getSize());
            }
        }
    }else pioche.piocher(&joueurs[joueurcourant].main,n);


}



void HuitAmericain::setCouleur(){

    if (joueurs[joueurcourant].humain){
        cout << "Choisissez une couleur (Ca,Co,P,T)"<<endl;
        getline (cin,couleurcourante);
        while(couleurcourante!="Ca"&&couleurcourante!="Co"&&couleurcourante!="P"&&couleurcourante!="T"){
            cout << "Couleur invalide, Choisissez une autre couleur (Ca,Co,P,T)"<<endl;
            getline (cin,couleurcourante);
        }
    }else{
        int ca = 0;
        int co = 0;
        int p = 0;
        int t = 0;
        string c ;
        for(int i=0;i<joueurs[joueurcourant].main.getSize();i++){
            c = joueurs[joueurcourant].main.getCouleur(i);
            if (c=="Ca") ca++;
            else if (c=="Co") co++;
            else if (c=="P") p++;
            else t++;
        }
        int n = ca;
        if (n<co) n=co;
        if (n<p) n = p;
        if (n<t) n = t;
        if(n==ca) couleurcourante="Ca";
        else if(n==co) couleurcourante="Co";
        else if(n==p) couleurcourante="P";
        else couleurcourante="T";



        cout << "L'ordinateur " << joueurs[joueurcourant].id << " a choisi la couleur " << couleurcourante <<"."<< endl;
    }

}
void HuitAmericain::tour_termine(bool js){

         cout << "Appuyez sur Entrée pour continuer" << endl;
        getchar();
        if(joueurs[joueurcourant].humain){
         system("clear");

    }if(js) joueursuivant();
}
void HuitAmericain::action(){

    int v = pile.getValeur(pile.getSize()-1);
    string c = pile.getCouleur(pile.getSize()-1);
     couleurcourante = c;
    if (v==8){
        if(joueurs[joueurcourant].main.getSize()!=0){
        setCouleur();
        }
        tour_termine();
        cout << "Changement de couleur, nouvelle couleur : " << couleurcourante <<endl<<endl;
    }else if (v==1){

        cartesapiocher+=2;
        affiche();
        tour_termine();

        if(joueurs[joueurcourant].humain){
            affiche();
            cout << "Le joueur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Joueur "<< joueurs[joueurcourant].id << " appuyez sur Entrée lorsque vous êtes prêt" <<endl<<endl;
           getchar();

            affiche();
            joueurs[joueurcourant].affiche(true);
            vector<int> coups = coups_possibles_contre();

            if(coups.size()==0){
                cout << "Aucune possibilité de contre, appuyez sur Entrée pour piocher les cartes" <<endl;
               getchar();
                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                joueurs[joueurcourant].affiche(true);
                tour_termine();
            }else{
                cout << "Choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher les cartes" <<endl;
                string carte; getline (cin,carte);
                int n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);

                while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                   cout << "n = " << n <<endl;
                    cout << "Carte invalide.De nouveau, choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher les cartes." <<endl;
                    getline (cin,carte);

                    n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                }if (carte==""){
                    cout << "Vous avez choisi de ne pas contrer." <<endl;
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    joueurs[joueurcourant].affiche(true);
                    tour_termine();
                }else{
                    int jc = joueurcourant;
                    joueurs[jc].main.placer(n,&pile);
                    v = pile.getValeur(pile.getSize()-1);
                    affiche();
                    if(v==8){
                        if(joueurs[joueurcourant].main.getSize()!=0){
                            setCouleur();
                        }
                        tour_termine();

                        cout << "Le joueur " <<  joueurs[jc].id << " a annulé la pénalité grace à un changement de couleur." << endl;
                        cartesapiocher = 0;
                    }else{tour_termine(false);
                        cout << "Le joueur " <<  joueurs[jc].id << " a contré la pénalité." << endl;
                        action();
                    }
                }

            }
        }else{
            cout << "L'ordinateur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Appuyez sur Entrée pour continuer" <<endl<<endl;
           getchar();

           /* affiche();
            joueurs[joueurcourant].affiche(true);*/
            vector<int> coups = coups_possibles_contre();
            if(coups.size()==0){

                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                joueurs[joueurcourant].affiche(true);
                tour_termine();
            }else{
                int jc = joueurcourant;
                int n = rand() % coups.size();
            joueurs[jc].main.placer(coups[n],&pile);
                    c = pile.getValeur(pile.getSize()-1);
                    affiche();
                    if(v==8){
                        if(joueurs[joueurcourant].main.getSize()!=0){
                            setCouleur();
                        }
                        tour_termine();

                        cout << "L'ordinateur "<<  joueurs[jc].id << " a annulé la pénalité grace à un changement de couleur." << endl; usleep(500000);
                        cartesapiocher = 0;
                    }else{tour_termine(false);
                        cout << "L'ordinateur " <<  joueurs[jc].id << " a contré la pénalité." << endl; usleep(500000);

                        action();
                    }
                }

            }

        }
    else if (v==2){
        tour_termine();
        if(joueurs[joueurcourant].humain){
            affiche();
            cout << "Le joueur "<< joueurs[joueurcourant].id << " doit piocher 2 cartes. Joueur "<< joueurs[joueurcourant].id << " appuyez sur Entrée lorsque vous êtes prêt" <<endl;
           getchar();
            joueurs[joueurcourant].affiche(true);
            cout << "Appuyez sur Entrée pour piocher les cartes." <<endl;
            getchar();
            piocher(2);
            affiche();
            joueurs[joueurcourant].affiche(true);
            tour_termine();
        }else{
            cout << "L'ordinateur " << joueurs[joueurcourant].id << " pioche 2 cartes" <<endl; usleep(500000);
            joueurs[joueurcourant].affiche(true);
            piocher(2);
            affiche();
            joueurs[joueurcourant].affiche(true);
            usleep(500000);
            tour_termine();
        }

    }else if (v==11){
        change_sens();
        tour_termine();
        cout << "Changement de sens, nouveau sens : " << sens <<endl<<endl;
    }else if (v==10){

        tour_termine(false);

        if(joueurs[joueurcourant].main.getSize()!=0){
            if (joueurs[joueurcourant].humain)
                cout << "Carte 10. Le joueur " ;
            else cout << "Carte 10. L'ordinateur ";
            cout <<  joueurs[joueurcourant].id << " doit de nouveau rejouer." << endl;
        }
    }else if (v==7){

        tour_termine();
        int js = joueurcourant;
        joueursuivant();
        if(joueurs[js].main.getSize()!=0){
            if (joueurs[js].humain)
                cout << "Le joueur " ;
            else cout << "L'ordinateur ";
            cout <<  joueurs[js].id << " passe son tour." << endl;
        }
    }

    else{

        tour_termine();

     }

}

void HuitAmericain::affiche(){   cout <<endl;
for (int i = 0;i<joueurs.size();i++){
        if (joueurs[i].humain)
            cout << "Joueur " ;
        else cout << "Ordinateur ";
        cout << joueurs[i].id << " -> Nombre de cartes restantes : " << joueurs[i].main.getSize() << " Score : " << joueurs[i].score <<endl;

}
    cout << "Sens : " << sens <<endl;
    cout << "Couleur courante : " << couleurcourante <<endl;
    pioche.affiche();pile.affiche();cout<<endl;

}

void HuitAmericain::affiche_regles(){   cout <<endl;
    cout << "Rappel des règles : " <<endl;
  cout << "À son tour, le joueur a le choix de jouer  soit une carte de la couleur de celle qui est en haut du talon, soit une carte de la même valeur, soit une carte joker à tout moment. Lorsqu'un joueur ne peut pas jouer de carte, il pioche une carte." <<endl;
  cout << "Cartes ayant un effet spécial : " <<endl
  << "- 8 : Joker, permet de jouer sur n'importe quelle carte et permet de choisir librement la couleur pour la suite du jeu; permet de stopper une attaque" <<endl
  << "- 1 : Fait piocher 2 cartes aux joueurs suivant, contrable avec uneautre carte 1 pour reporter l'attaque au joueur suivant ou un 8 pour changer de couleur" <<endl
  << "- 2 : Même effet que le 1, mais non contrable" <<endl
  << "- Valet : Change le sens du jeu " <<endl
  << "- 10 : Oblige le joueur à rejouer " <<endl
  << "- 7 : Saute le tour du joueur suivant"<<endl <<endl;


}





void HuitAmericain::jouer(){
    while (gagne()==-1){

        cout << "Début manche  " << nummanche << endl;
        while(gagne_manche()==-1){
        affiche();
        cout<<endl<<endl;
        int id = joueurs[joueurcourant].id;
        //cout << "Joueur courant  " << joueurcourant <<" id " << id << endl;
        if (joueurs[joueurcourant].humain){
            cout<<"Joueur "<< joueurs[joueurcourant].id << " c'est à vous de jouer. Appuyez sur Entrée lorsque vous êtes prêt";getchar();
        }else{ cout<<"C'est à l'ordinateur "<< joueurs[joueurcourant].id << " de jouer. Appuyez sur Entrée pour continuer.";
          getchar();}
            affiche();
            joueurs[joueurcourant].affiche(true);
            vector<int> coups = coups_possibles();
           /* for (int i=0;i<coups.size();i++)
                cout << coups[i]<< " ";
            cout<<endl;*/
         if (joueurs[joueurcourant].humain){
            if(coups.size()==0){
                cout<< "Pas de coups possible, appuyez sur Entrée pour piocher une carte"<<endl;
                getchar();
                piocher();
                affiche();
                joueurs[joueurcourant].affiche(true);
                coups = coups_possibles();
                if(coups.size()!=0){
                    cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour terminer votre tour."<<endl;
                    string carte; getline (cin,carte);
                    int n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                    while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                   // cout << "n = " << n <<endl;
                            cout << "Carte invalide.De nouveau, choisissez une carte puis appuyez sur entrée, ou appuyez juste sur Entrée pour terminer votre tour." <<endl;
                            getline (cin,carte);
                            n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                        }if (carte==""){
                            joueursuivant();
                            system("clear");
                            cout << "Le joueur " << id << "a choisi de passer son tour" << endl;
                        }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            affiche();
                            action();
                        }
                }else{
                    cout << "Aucun coup possible, appuyez sur Entrée pour terminer votre tour." << endl;
                    getchar();
                    joueursuivant();
                    system("clear");
                    cout << "Le joueur " << id << "a pioché une carte" << endl;
                }
            }else{
               cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour piocher."<<endl;
               string carte; getline (cin,carte);
                int n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                   // cout << "n = " << n <<endl;
                    cout << "Carte invalide.De nouveau, choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher les cartes." <<endl;
                    getline (cin,carte);
                    n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                }if (carte==""){
                    piocher();
                    joueurs[joueurcourant].affiche(true);
                        cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour terminer votre tour."<<endl;
                        getline (cin,carte);
                        n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                        while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                            cout << "Carte invalide.De nouveau, choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste surEntrée pour terminer votre tour." <<endl;
                            getline (cin,carte);
                            n = CarteClassique::recherche(carte,&joueurs[joueurcourant].main);
                        }if (carte==""){
                            joueursuivant();
                            system("clear");
                            cout << "Le joueur " << id << "a choisi de passer son tour" << endl;
                        }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            couleurcourante = joueurs[joueurcourant].main.getCouleur(n);
                            affiche();
                            action();

                        }


                }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            affiche();
                           action();
                }
            }
        }else{
             usleep(500000);
            if(coups.size()==0){
                cout<< "Pas de coups possible, l'ordinateur pioche"<<endl;
                usleep(500000);
                piocher();
                affiche();
                joueurs[joueurcourant].affiche(true);
                usleep(500000);
                coups = coups_possibles();
                if(coups.size()!=0){
                            int n = joueurs[joueurcourant].main.getSize()-1;
                            joueurs[joueurcourant].main.placer(n,&pile);
                           // couleurcourante = joueurs[joueurcourant].main.getCouleur(n);
                            affiche();
                            action();

                }else{
                    cout << "Aucun coup possible pour l'ordinateur, appuyez sur Entrée pour continuer." << endl;
                    getchar();
                    joueursuivant();

                }
            }else{
                            int n = rand() % coups.size();
                            joueurs[joueurcourant].main.placer(coups[n],&pile);
                            affiche();
                           action();
                }



        }
    }calcul_score(gagne_manche());
    cout << "Manche terminée, résultats :" <<endl;
    for (int i=0; i<joueurs.size();i++){
        joueurs[i].affiche(false);
    }tour_termine();
    nummanche++;
    initialisation();
    }cout << "Limite de points atteinte, jeu terminé." <<endl; affiche_gagnant(gagne());
}



HuitAmericain::~HuitAmericain()
{
    //dtor
}
