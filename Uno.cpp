#include "Uno.h"

#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
using namespace std;

Uno::Uno(int j, int o,int p):JeudeCartes(j,o)
{

if(j+o<2||j+o>5) throw string("2 à 5 joueurs recquis");
    points = p;

    initialisation();

}

void Uno::initialisation(){
    melange();
    sens="horaire";
    joueurcourant=0;
    pioche.vider();
    pile.vider();
    for(int i=0;i<joueurs.size();i++){
            joueurs[i].main.vider();
    }

    pioche.ajouter(new CarteUno(0,"R"));
    pioche.ajouter(new CarteUno(0,"B"));
    pioche.ajouter(new CarteUno(0,"J"));
    pioche.ajouter(new CarteUno(0,"V"));
    for(int j=0;j<2;j++){
    for(int i = 1;i<10;i++){
            pioche.ajouter(new CarteUno(i,"R",i));
            pioche.ajouter(new CarteUno(i,"B",i));
            pioche.ajouter(new CarteUno(i,"J",i));
            pioche.ajouter(new CarteUno(i,"V",i));
        }
    }
    for(int i = 10;i<13;i++){
            pioche.ajouter(new CarteUno(i,"R",20));
            pioche.ajouter(new CarteUno(i,"B",20));
            pioche.ajouter(new CarteUno(i,"J",20));
            pioche.ajouter(new CarteUno(i,"V",20));
        }


     for(int i = 0;i<4;i++){
            pioche.ajouter(new CarteUno(13,"",50)); //Joker
            pioche.ajouter(new CarteUno(14,"",50)); //+4
    }

        pioche.melange();
        pioche.piocher(&pile);
        couleurcourante = pile.getCouleur(pile.getSize()-1);
        for (int i=0;i<joueurs.size();i++){
            pioche.piocher(&joueurs[i].main,7);
        }


}

void Uno::calcul_score(int gagnant){
int v;int s;
    for (int i=0;i<joueurs.size();i++){
        for (int j=0;j<joueurs[i].main.getSize();j++){
            s = joueurs[i].main.getGain(j);
            joueurs[gagnant].score +=s;
        }

    }
}

int Uno::gagne_manche(){
    for(int i=0;i<joueurs.size();i++){
        if (joueurs[i].main.getSize() == 0) return i;
    }
    return -1;
}

int Uno::gagne(){
    for(int i=0;i<joueurs.size();i++){
        if (joueurs[i].score >= points) return i;
    }return -1;
}

void Uno::joueursuivant(){
    if (sens=="horaire"){
        if(joueurcourant==(joueurs.size()-1))
            joueurcourant = 0;
        else joueurcourant++;
    }else{
        if(joueurcourant==0)
            joueurcourant = joueurs.size()-1;
        else joueurcourant--;
    }

}

vector<int> Uno::coups_possibles(bool compte4){
    int v = pile.getValeur(pile.getSize()-1);
    string c = pile.getCouleur(pile.getSize()-1);
    vector<int> coups;
    for (int i = 0; i<joueurs[joueurcourant].main.getSize();i++){
        if(joueurs[joueurcourant].main.getValeur(i)==13)
            coups.push_back(i);
        else if(joueurs[joueurcourant].main.getValeur(i)==14){
            if (compte4) coups.push_back(i);
        }
        else{
            if(v==joueurs[joueurcourant].main.getValeur(i))
                coups.push_back(i);
            else if (joueurs[joueurcourant].main.getCouleur(i)==couleurcourante)
                coups.push_back(i);
        }
    }
    return coups;
}

vector<int> Uno::coups_possibles_contre(){
    vector<int> coups;
    for (int i = 0; i<joueurs[joueurcourant].main.getSize();i++){
        if(joueurs[joueurcourant].main.getValeur(i)==10)
                coups.push_back(i);
    }

    return coups;
}

void Uno::piocher(int n){
    if (pioche.getSize()<n){
        pile.deplacertout(&pioche);
        pioche.piocher(&pile);
        if(pioche.getSize()==0){
            cout << "Pioche vide. Piochage annulé." << endl;
        }else{
            if (pioche.getSize()>=n){
                pioche.piocher(&joueurs[joueurcourant].main,n);
            }else{
                cout << "Pas assez de cartes dans la pioche, piochage de " << pile.getSize() << " cartes"<<endl;
                pioche.piocher(&joueurs[joueurcourant].main,pile.getSize());
            }
        }
    }else pioche.piocher(&joueurs[joueurcourant].main,n);


}


void Uno::setCouleur(){

    if (joueurs[joueurcourant].humain){
        cout << "Joueur "<< joueurs[joueurcourant].id << ", choisissez une couleur (R,B,V,J)"<<endl;
        getline (cin,couleurcourante);
        while(couleurcourante!="R"&&couleurcourante!="B"&&couleurcourante!="V"&&couleurcourante!="J"){
            cout << "Couleur invalide, Choisissez une autre couleur (R,B,V,J)"<<endl;
            getline (cin,couleurcourante);
        }
    }else{
        int ca = 0;
        int co = 0;
        int p = 0;
        int t = 0;
        string c ;
        for(int i=0;i<joueurs[joueurcourant].main.getSize();i++){
            c = joueurs[joueurcourant].main.getCouleur(i);
            if (c=="R") ca++;
            else if (c=="B") co++;
            else if (c=="V") p++;
            else t++;
        }
        int n = ca;
        if (n<co) n=co;
        if (n<p) n = p;
        if (n<t) n = t;
        if(n==ca) couleurcourante="R";
        else if(n==co) couleurcourante="B";
        else if(n==p) couleurcourante="V";
        else couleurcourante="J";



        cout << "L'ordinateur " << joueurs[joueurcourant].id << " a choisi la couleur " << couleurcourante <<"."<< endl;
    }

}
void Uno::tour_termine(bool js){

         cout << "Appuyez sur Entrée pour continuer" << endl;
        getchar();
        if(joueurs[joueurcourant].humain){
         system("clear");

    }if(js) joueursuivant();
}
void Uno::action(){

     couleurcourante = pile.getCouleur(pile.getSize()-1);
      //cout<<"etape 1 "<<endl;
    if (pile.getValeur(pile.getSize()-1)==13){
        if(joueurs[joueurcourant].main.getSize()!=0){
        setCouleur();
        }
        tour_termine();
        cout << "Changement de couleur, nouvelle couleur : " << couleurcourante <<endl<<endl;
    }else if (pile.getValeur(pile.getSize()-1)==10){

        cartesapiocher+=2;
        affiche();
        tour_termine();

        if(joueurs[joueurcourant].humain){
            affiche();
            cout << "Le joueur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Joueur "<< joueurs[joueurcourant].id << " appuyez sur Entrée lorsque vous êtes prêt" <<endl<<endl;
           getchar();

            affiche();
            joueurs[joueurcourant].affiche(true);
            vector<int> coups = coups_possibles_contre();

            if(coups.size()==0){
                cout << "Aucune possibilité de contre, appuyez sur Entrée pour piocher les cartes" <<endl;
               getchar();
                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                joueurs[joueurcourant].affiche(true);
                tour_termine();
            }else{
                cout << "Choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher les cartes" <<endl;
                string carte; getline (cin,carte);
                int n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);

                while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){

                    cout << "Carte invalide.De nouveau, choisissez une carte pour contrer puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher les cartes." <<endl;
                    getline (cin,carte);

                    n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                }if (carte==""){
                    cout << "Vous avez choisi de ne pas contrer." <<endl;
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    joueurs[joueurcourant].affiche(true);
                    tour_termine();
                }else{
                    int jc = joueurcourant;
                    joueurs[jc].main.placer(n,&pile);

                    affiche();
                    tour_termine(false);
                    cout << "Le joueur " <<  joueurs[jc].id << " a contré la pénalité." << endl;
                    action();

                }

            }
        }else{
            cout << "L'ordinateur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Appuyez sur Entrée pour continuer" <<endl<<endl;
           getchar();

           /* affiche();
            joueurs[joueurcourant].affiche(true);*/
            vector<int> coups = coups_possibles_contre();
            if(coups.size()==0){

                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                joueurs[joueurcourant].affiche(true);
                tour_termine();
            }else{
                int jc = joueurcourant;
                int n = rand() % coups.size();
            joueurs[jc].main.placer(coups[n],&pile);

                    affiche();
                    tour_termine(false);
                        cout << "L'ordinateur " <<  joueurs[jc].id << " a contré la pénalité." << endl; usleep(500000);

                        action();

                }

            }

        }else if (pile.getValeur(pile.getSize()-1)==14){

        cartesapiocher+=4;
        int jp = joueurcourant;
        affiche();
        pile.supprimer();
        vector<int> coups = coups_possibles(false);
        pile.ajouter(new CarteUno(14,""));
        if(joueurs[jp].main.getSize()!=0){
        setCouleur();
        }
        tour_termine();

        if(joueurs[joueurcourant].humain){
            affiche();
            cout << "Le joueur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Joueur "<< joueurs[joueurcourant].id << " appuyez sur Entrée lorsque vous êtes prêt" <<endl<<endl;
           getchar();

            string conteste;
            cout << "Accepter ou contester la pénalité? (1:Accepter 2:Contester)" <<endl;
            getline (cin,conteste);


                while(conteste!="1"&&conteste!="2"){
                    cout << "Entrez 1 ou 2" <<endl;
                    getline (cin,conteste);
                }


            if(conteste=="1"){
                cout << "Pénalité accepté, vous piochez 4 cartes." <<endl;
               getchar();
                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                tour_termine();
            }else{
                if(coups.size()==0){
                    cout << "Contestation échouée, vous piochez 2 cartes supplémentaires."<<endl;
                    cartesapiocher +=2;
                    getchar();
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    tour_termine();
                }else{
                    joueurcourant =jp;
                    cout << "Contestation réussie, ";
                    if (joueurs[joueurcourant].humain)
                        cout << "le joueur ";
                    else
                        cout << "l'ordinateur ";
                    cout << joueurs[joueurcourant].id << " pioche 4 cartes.";
                    getchar();
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    tour_termine();

                }

            }
        }else{
            affiche();
            cout << "L'ordinateur "<< joueurs[joueurcourant].id << " doit piocher " << cartesapiocher << " cartes. Appuyez sur Entrée pour continuer" <<endl<<endl;
           getchar();

            int conteste;
            conteste = rand() % 2;

            if(conteste==0){
                cout << "L'ordinateur a accepté la pénalité." <<endl;
               getchar();
                piocher(cartesapiocher);
                cartesapiocher = 0;
                affiche();
                tour_termine();
            }else{
                cout << "L'ordinateur a contesté la pénalité." <<endl;
                usleep(500000);
                if(coups.size()==0){
                    cout << "Contestation échouée, l'ordinateur pioche 2 cartes supplémentaires."<<endl;
                    cartesapiocher +=2;
                    getchar();
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    tour_termine();
                }else{
                    joueurcourant =jp;
                    cout << "Contestation réussie, ";
                    if (joueurs[jp].humain)
                        cout << "le joueur ";
                    else
                        cout << "l'ordinateur ";
                    cout << joueurs[jp].humain << " pioche 4 cartes.";
                    cartesapiocher +=2;
                    getchar();
                    piocher(cartesapiocher);
                    cartesapiocher = 0;
                    affiche();
                    tour_termine();

                }

            }

        }
    }else if (pile.getValeur(pile.getSize()-1)==11){
        change_sens();
        tour_termine();
        cout << "Changement de sens, nouveau sens : " << sens <<endl<<endl;
    }else if (pile.getValeur(pile.getSize()-1)==12){

        tour_termine();
        int js = joueurcourant;
        joueursuivant();
        if(joueurs[js].main.getSize()!=0){
            if (joueurs[js].humain)
                cout << "Le joueur " ;
            else cout << "L'ordinateur ";
            cout <<  joueurs[js].id << " passe son tour." << endl;
        }
    }

    else{

        tour_termine();

     }

}

void Uno::affiche(){   cout <<endl;
for (int i = 0;i<joueurs.size();i++){
        if (joueurs[i].humain)
            cout << "Joueur " ;
        else cout << "Ordinateur ";
        cout << joueurs[i].id << " -> Nombre de cartes restantes : " << joueurs[i].main.getSize() << " Score : " << joueurs[i].score <<endl;

}
    cout << "Sens : " << sens <<endl;
    cout << "Couleur courante : " << couleurcourante <<endl;
    pioche.affiche();pile.affiche();cout<<endl;

}

void Uno::affiche_regles(){   cout <<endl;
    cout << "Rappel des règles : " <<endl;
  cout << "À son tour, le joueur a le choix de jouer  soit une carte de la couleur de celle qui est en haut du talon, soit une carte de la même valeur, soit une carte joker ou +4 à tout moment. Lorsqu'un joueur ne peut pas jouer de carte, il pioche une carte." <<endl;
  cout << "Cartes ayant un effet spécial : " <<endl
  << "- J : Joker, permet de jouer sur n'importe quelle carte et permet de choisir librement la couleur pour la suite du jeu" <<endl
  << "- +2 : Fait piocher 2 cartes aux joueurs suivant, contrable avec uneautre carte 1 pour reporter l'attaque au joueur suivant ou un 8 pour changer de couleur" <<endl
  << "- +4: Peut-être joué sur n'importe quel carte .Fait piocher 4 cartes au joueur suivant et de changer de couleur. En cas de contestation si le joueur qui a posé la carte avait un autre coup possible, celui ci pioche 4 cartes, sinon le joueur suivant pioche 2 cartes supplémentaires." <<endl
  << "- I : Change le sens du jeu " <<endl
  << "- P : Saute le tour du joueur suivant"<<endl <<endl;


}





void Uno::jouer(){
    while (gagne()==-1){

        cout << "Début manche  " << nummanche << endl;
        if(pile.getValeur(pile.getSize()-1)==13||pile.getValeur(pile.getSize()-1)==14){
        affiche();
        setCouleur();}
        while(gagne_manche()==-1){
        affiche();
        cout<<endl<<endl;
        int id = joueurs[joueurcourant].id;
        //cout << "Joueur courant  " << joueurcourant <<" id " << id << endl;
        if (joueurs[joueurcourant].humain){
            cout<<"Joueur "<< joueurs[joueurcourant].id << " c'est à vous de jouer. Appuyez sur Entrée lorsque vous êtes prêt";getchar();
        }else{ cout<<"C'est à l'ordinateur "<< joueurs[joueurcourant].id << " de jouer. Appuyez sur Entrée pour continuer.";
          getchar();}
            affiche();
            joueurs[joueurcourant].affiche(true);
            vector<int> coups = coups_possibles();
           /* for (int i=0;i<coups.size();i++)
                cout << coups[i]<< " ";
            cout<<endl;*/
         if (joueurs[joueurcourant].humain){
            if(coups.size()==0){
                cout<< "Pas de coups possible, appuyez sur Entrée pour piocher une carte"<<endl;
                getchar();
                piocher();
                affiche();
                joueurs[joueurcourant].affiche(true);
                coups = coups_possibles();
                if(coups.size()!=0){
                    cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour terminer votre tour."<<endl;
                    string carte; getline (cin,carte);

                    int n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                    while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                   // cout << "n = " << n <<endl;
                            cout << "Carte invalide.De nouveau, choisissez une carte puis appuyez sur entrée, ou appuyez juste sur Entrée pour terminer votre tour." <<endl;
                            getline (cin,carte);
                            n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                        }if (carte==""){
                            joueursuivant();
                            system("clear");
                            cout << "Le joueur " << id << "a choisi de passer son tour" << endl;
                        }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            affiche();
                            action();
                        }
                }else{
                    cout << "Aucun coup possible, appuyez sur Entrée pour terminer votre tour." << endl;
                    getchar();
                    joueursuivant();
                    system("clear");
                    cout << "Le joueur " << id << "a pioché une carte" << endl;
                }
            }else{
               cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour piocher."<<endl;
               string carte; getline (cin,carte);
               //cout<<"carte choisie :" << carte <<":"<<endl;
                int n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                   // cout << "n = " << n <<endl;
                    cout << "Carte invalide.De nouveau, choisissez une carte puis appuyez sur entrée, ou appuyez juste sur Entrée pour piocher." <<endl;
                    getline (cin,carte);
                    cout<<"carte choisie :" << carte <<":"<<endl;
                    n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                }if (carte==""){
                    piocher();
                    joueurs[joueurcourant].affiche(true);
                    coups = coups_possibles();
                        cout<< "Choisissez la carte à jouer puis appuyez sur Entrée, ou appuyez juste sur Entrée pour terminer votre tour."<<endl;
                        getline (cin,carte);
                        n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                        while(carte!=""&&(n==-1||(find(coups.begin(), coups.end(), n)==coups.end()))){
                            cout << "Carte invalide.De nouveau, choisissez une carte puis appuyez sur entrée, ou appuyez juste sur Entrée pour terminer votre tour." <<endl;
                            getline (cin,carte);
                            n = CarteUno::recherche(carte,&joueurs[joueurcourant].main);
                        }if (carte==""){
                            joueursuivant();
                            system("clear");
                            cout << "Le joueur " << id << "a choisi de passer son tour" << endl;
                        }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            if(joueurs[joueurcourant].main.getValeur(n)!=13&&joueurs[joueurcourant].main.getValeur(n)!=14)
                            couleurcourante = joueurs[joueurcourant].main.getCouleur(n);
                            affiche();
                            action();

                        }


                }else{
                            joueurs[joueurcourant].main.placer(n,&pile);
                            if(joueurs[joueurcourant].main.getValeur(n)!=13&&joueurs[joueurcourant].main.getValeur(n)!=14)
                            couleurcourante = joueurs[joueurcourant].main.getCouleur(n);
                            affiche();
                           action();
                }
            }
        }else{
             usleep(500000);
            if(coups.size()==0){
                cout<< "Pas de coups possible, l'ordinateur pioche"<<endl;
                usleep(500000);
                piocher();
                affiche();
                joueurs[joueurcourant].affiche(true);
                usleep(500000);
                coups = coups_possibles();
                if(coups.size()!=0){
                            int n = joueurs[joueurcourant].main.getSize()-1;
                            joueurs[joueurcourant].main.placer(n,&pile);

                            /*if(joueurs[joueurcourant].main.getValeur(n)!=13&&joueurs[joueurcourant].main.getValeur(n)!=14){
                            couleurcourante = joueurs[joueurcourant].main.getCouleur(n);cout<<"etape 2 "<<endl;
                            }*/
                            affiche();

                            action();


                }else{
                    cout << "Aucun coup possible pour l'ordinateur, appuyez sur Entrée pour continuer." << endl;
                    getchar();
                    joueursuivant();

                }
            }else{
                            int n = rand() % coups.size();
                            joueurs[joueurcourant].main.placer(coups[n],&pile);
                            affiche();
                           action();
                }



        }
    }calcul_score(gagne_manche());
    cout << "Manche terminée, résultats :" <<endl;
    for (int i=0; i<joueurs.size();i++){
        joueurs[i].affiche(false);
    }tour_termine();
    nummanche++;
    initialisation();
    }cout << "Limite de points atteinte, jeu terminé." <<endl; affiche_gagnant(gagne());
}



Uno::~Uno()
{
    //dtor
}
