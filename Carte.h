#ifndef CARTE_H
#define CARTE_H
#include <iostream>
#include <string>
#include "PileDeCartes.h"
class PileDeCartes;
class Carte
{
public:
    Carte(int,std::string,int=0,bool v = true);
    virtual ~Carte();
    virtual void affiche() const =0;
    std::string getCouleur() const;
    int getValeur() const;
    int getGain() const;
    void setVisible(bool);
    Carte(const Carte& other);
protected:
    const int valeur;
    const std::string couleur;
    int gain;
    bool visible=true;
private:
};

#endif // CARTE_H
