#include "CarteUno.h"
#include <stdio.h>
#include <vector>
#include <cstdlib>
#include <unistd.h>
using namespace std;
CarteUno::CarteUno(int v,string c,int g,bool vi):Carte(v,c,g,vi)
{
    //ctor
}



void CarteUno::affiche() const
{
    if (visible){
    if (valeur <10){
        cout<<valeur;
    }else if (valeur==10){
        cout << "+2";
    }else if (valeur==11){
        cout<<"I";
    }else if (valeur==12){cout<<"P";}
    else if (valeur==13) {cout<<"J";}
    else if (valeur==14){cout <<"+4";}
    cout << couleur<< " ";
    }else cout<< "X" << " ";
}

int CarteUno::recherche(std::string carte,PileDeCartes* p){
    for (int i=0;i<p->getSize();i++){
        if (std::to_string(p->getValeur(i)) + p->getCouleur(i)== carte){
                return i;
        }else if(p->getValeur(i)==10){
            if ("+2" + p->getCouleur(i)== carte)
                return i;
        }else if(p->getValeur(i)==11){
            if ("I" + p->getCouleur(i)== carte)
            return i;
        }else if(p->getValeur(i)==12){
            if ("P" + p->getCouleur(i)== carte)
            return i;
        }else if(p->getValeur(i)==13){
            if ("J"  == carte)
            return i;
        }else if(p->getValeur(i)==14){
            if ("+4"  == carte)
            return i;
        }
    }return -1;

}
CarteUno::~CarteUno()
{
    //dtor
}

CarteUno::CarteUno(const CarteUno& x):Carte(x){
    //copy ctor
}
