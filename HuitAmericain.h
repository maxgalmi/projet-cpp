#ifndef HUITAMERICAIN_H
#define HUITAMERICAIN_H
#define BATAILLE_H
#include "JeudeCartes.h"
#include "CarteClassique.h"
#include <iostream>
#include <string>
#include <vector>

class HuitAmericain : public JeudeCartes
{
    public:
        HuitAmericain(int,int,int=100);
        int points;
        std::string sens = "horaire";
        std::string couleurcourante;
        PileDeCartes pile = PileDeCartes("pile");
        int cartesapiocher=0;
        void initialisation();
        virtual void affiche();
        void affiche_regles();
        virtual void jouer();
        virtual int gagne();
        virtual int gagne_manche();
        void piocher(int=1);
        void action();
        void tour_termine(bool=true);
        void calcul_score(int gagnant);
        std::vector<int> coups_possibles();
        std::vector<int> coups_possibles_contre();
        void setCouleur();
        virtual ~HuitAmericain();
    protected:
    private:
};

#endif // HUITAMERICAIN_H
