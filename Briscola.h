#ifndef BRISCOLA_H
#define BRISCOLA_H
#include "JeudeCartes.h"
#include "CarteItal.h"


class Briscola: public JeudeCartes
{
public:
    Briscola(int,int);
      void jouer();
      void affiche_regles();
    virtual ~Briscola();

private:
    PileDeCartes tapis = PileDeCartes("tapis");
    PileDeCartes briscopile = PileDeCartes("pile");
    void affiche();
    void afficheBriscola();
    void initialisation ();
    int gagne();
    int gagne_tour(PileDeCartes &p);
    int gagne_manche();
    void piocher(int=1);
    void calcul_score();
    void tour_termine(bool=true);

};

#endif // BRISCOLA_H
