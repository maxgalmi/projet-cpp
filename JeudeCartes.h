#ifndef JEUDECARTES_H
#define JEUDECARTES_H
#include "Joueur.h"
#include <iostream>
#include <algorithm>
#include <vector>

class JeudeCartes
{
    public:


        virtual void affiche_regles()=0;
        virtual void jouer() =0;


        JeudeCartes(int,int);
        virtual ~JeudeCartes();
    protected:
        std::string sens;
        std::vector<Joueur> joueurs;
        int joueurcourant=0;
        int nummanche=1;
        virtual void affiche()=0;
        virtual void tour_termine(bool=true)=0;
        virtual int gagne_manche() = 0;
        PileDeCartes pioche = PileDeCartes("pioche");
        void melange();
        void change_sens();
        void joueursuivant();
        virtual void initialisation() = 0;
        void affiche_gagnant(int);
    private:

};

#endif // JEUDECARTES_H
