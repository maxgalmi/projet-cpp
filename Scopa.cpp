#include "Scopa.h"
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
using namespace std;

Scopa::Scopa(int j, int o):JeudeCartes(j,o)
{
    if(j+o<1||j+o>2) throw string("1 à 2 joueurs recquis");
    cout << "Yep"<<endl;
    initialisation();

}

void Scopa::initialisation()
{
    melange();
    joueurcourant=0;
    while(pioche.getSize()!=0)
        pioche.supprimer();
    while(tapis.getSize()!=0)
        tapis.supprimer();
    for(int i=0; i<joueurs.size(); i++)
    {
        while(joueurs[i].main.getSize()!=0)
            joueurs[i].main.supprimer();
    }

    pioche.ajouter(new CarteItal(1,"Ep",16));
    pioche.ajouter(new CarteItal(1,"Co",16));
    pioche.ajouter(new CarteItal(1,"Ba",16));
    pioche.ajouter(new CarteItal(1,"De",16));

    pioche.ajouter(new CarteItal(2,"Ep",12));
    pioche.ajouter(new CarteItal(2,"Co",12));
    pioche.ajouter(new CarteItal(2,"Ba",12));
    pioche.ajouter(new CarteItal(2,"De",12));

    pioche.ajouter(new CarteItal(3,"Ep",13));
    pioche.ajouter(new CarteItal(3,"Co",13));
    /*
       pioche.ajouter(new CarteItal(3,"Ba",13));
       pioche.ajouter(new CarteItal(3,"De",13));

       pioche.ajouter(new CarteItal(4,"Ep",14));
       pioche.ajouter(new CarteItal(4,"Co",14));
       pioche.ajouter(new CarteItal(4,"Ba",14));
       pioche.ajouter(new CarteItal(4,"De",14));

       pioche.ajouter(new CarteItal(5,"Ep",15));
       pioche.ajouter(new CarteItal(5,"Co",15));
       pioche.ajouter(new CarteItal(5,"Ba",15));
       pioche.ajouter(new CarteItal(5,"De",15));

       pioche.ajouter(new CarteItal(6,"Ep",18));
       pioche.ajouter(new CarteItal(6,"Co",18));
       pioche.ajouter(new CarteItal(6,"Ba",18));
       pioche.ajouter(new CarteItal(6,"De",18));

       pioche.ajouter(new CarteItal(7,"Ep",21));
       pioche.ajouter(new CarteItal(7,"Co",21));
       pioche.ajouter(new CarteItal(7,"Ba",21));
       pioche.ajouter(new CarteItal(7,"De",21));
       for(int i = 8; i<11; i++)
       {
           pioche.ajouter(new CarteItal(i,"Ep",10));
           pioche.ajouter(new CarteItal(i,"Co",10));
           pioche.ajouter(new CarteItal(i,"Ba",10));
           pioche.ajouter(new CarteItal(i,"De",10));
       }
       */

    pioche.melange();
    for (int i=0; i<joueurs.size(); i++)
    {
        pioche.piocher(&joueurs[i].main,3);
    }
    do
    {
        for(int i=0; i<4; i++)
        {
            pioche.piocher(&tapis);
        }
        if (!troisRois(tapis))
        {
            for(int i=0; i<4; i++)
            {
                tapis.piocher(&pioche);
            }
            pioche.melange();
        }
    }
    while(!troisRois(tapis));
}

bool Scopa::troisRois(PileDeCartes &p)
{
    int compteur=0;
    bool b=true;
    for(int i=0; i<p.getSize(); i++)
    {
        if (p.getValeur(i)==10)
        {
            compteur++;
        }
    }
    if (compteur>=3 || p.getSize()<4)
    {
        b=false;
    }
    return b;
}


void Scopa::affiche_regles()
{
    cout <<endl;
    cout << "Google, c'est genial : " <<endl;
}

void Scopa::max_cartes()
{
    int player;
    int tmp=0;
    for (int i=0; i<joueurs.size(); i++)
    {
        if (joueurs[i].plis.getSize()>tmp)
        {
            player=i;
            tmp=joueurs[i].plis.getSize();
        }
    }
    joueurs[player].score++;
    cout << "Le joueur " << joueurs[player].id << " remporte un point pour avoir le plus grand nombre de cartes" << endl;

}

void Scopa::max_de()
{
    int tmp=0;
    int player;
    int tab[joueurs.size()-1];
    for (int i=0; i<joueurs.size(); i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getCouleur(j)=="De")
            {
                tab[i]++;
            }

            for (int i=0; i<joueurs.size(); i++)
            {
                if (tab[i]>tmp)
                {
                    tmp=tab[i];
                    player =i;
                }
            }

        }
    }
    joueurs[player].score++;
    cout << "Le joueur " << joueurs[player].id << " remporte un point pour avoir le plus de deniers" << endl;
}

void Scopa::max_septde()
{
    int player;
    for (int i=0; i<joueurs.size(); i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getValeur(j)==7 && joueurs[i].plis.getCouleur(j)=="De")
                player=i;
            else return;
        }
    }
    joueurs[player].score++;
    cout << "Le joueur " << joueurs[player].id << " remporte un point pour avoir le 7 de deniers" << endl;

}

void Scopa::max_chaqueCouleur()
{

    // Un peu d'humour pour la derniere méthode conçue , qui traduit notre desespoir

    int tailleJou=joueurs.size();
    int ptsEp[tailleJou];
    int ptsDe[tailleJou];
    int ptsCo[tailleJou];
    int ptsBa[tailleJou];

    int gagnantEp;
    int gagnantCo;
    int gagnantDe;
    int gagnantBa;

    int tmpEp=0;
    int tmpCo=0;
    int tmpDe=0;
    int tmpBa=0;


    for (int i=0; i<tailleJou; i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getCouleur(j)=="Ep")
                ptsEp[i]+=joueurs[i].plis.getGain(j);
        }
    }

    for (int i=0; i<tailleJou; i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getCouleur(j)=="Ba")
                ptsBa[i]+=joueurs[i].plis.getGain(j);
        }
    }

    for (int i=0; i<tailleJou; i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getCouleur(j)=="De")
                ptsDe[i]+=joueurs[i].plis.getGain(j);
        }
    }

    for (int i=0; i<tailleJou; i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            if (joueurs[i].plis.getCouleur(j)=="Co")
                ptsCo[i]+=joueurs[i].plis.getGain(j);
        }
    }

    for (int i=0; i<tailleJou; i++)
        if (ptsDe[i]>tmpDe)
        {
            tmpDe=ptsDe[i];
            gagnantDe=i;
        }

    for (int i=0; i<tailleJou; i++)
        if (ptsCo[i]>tmpCo)
        {
            tmpCo=ptsCo[i];
            gagnantCo=i;
        }

    for (int i=0; i<tailleJou; i++)
        if (ptsEp[i]>tmpEp)
        {
            tmpEp=ptsEp[i];
            gagnantEp=i;
        }

    for (int i=0; i<tailleJou; i++)
        if (ptsBa[i]>tmpBa)
        {
            tmpBa=ptsBa[i];
            gagnantBa=i;
        }
    if (tmpBa!=0)
    {
        joueurs[gagnantBa].score++;
        cout << "Le joueur " << joueurs[gagnantBa].id << " remporte un point pour avoir le plus grand nombre de batons" << endl;
    }
    if (tmpDe!=0)
    {
        joueurs[gagnantDe].score++;
        cout << "Le joueur " << joueurs[gagnantDe].id << " remporte un point pour avoir le plus grand nombre de deniers" << endl;
    }
    if (tmpEp!=0)
    {
        joueurs[gagnantEp].score++;
        cout << "Le joueur " << joueurs[gagnantEp].id << " remporte un point pour avoir le plus grand nombre d'epees" << endl;
    }
    if (tmpCo!=0)
    {
        joueurs[gagnantCo].score++;
        cout << "Le joueur " << joueurs[gagnantCo].id << " remporte un point pour avoir le plus grand nombre de coupes" << endl;
    }
}

void Scopa::calcul_score()
{
    max_septde();
    max_de();
    max_cartes();
    max_chaqueCouleur();
}


int Scopa::gagne_manche()
{

}

void Scopa::match_carte()
{
    int Sansdernier=tapis.getSize()-1;
    int dernier=tapis.getValeur(Sansdernier);


    for (int i=0; i<Sansdernier; i++)
    {
        if (tapis.getValeur(i)==dernier)
        {
            tapis.placer(Sansdernier,&joueurs[joueurcourant].plis);
            tapis.placer(i,&joueurs[joueurcourant].plis);
            dernierMangeur=joueurs[joueurcourant].id;
            return;
        }
    }

    for (int i=0; i<Sansdernier ; i++)
    {
        for(int j=i+1; j<Sansdernier ; j++)
            if (tapis.getValeur(i)+tapis.getValeur(j)==dernier)
            {
                tapis.placer(Sansdernier,&joueurs[joueurcourant].plis);
                tapis.placer(i,&joueurs[joueurcourant].plis);
                tapis.placer(j,&joueurs[joueurcourant].plis);
                dernierMangeur=joueurs[joueurcourant].id;
                return;
            }
    }
    return;
}

int Scopa::gagne()
{
    int tmp(0);
    int j;
    for(int i=0; i<joueurs.size(); i++)
    {
        if (joueurs[i].score>tmp)
        {
            tmp=joueurs[i].score;
            j=i;
        }
        return joueurs[j].id;
    }
    return -1;
}



void Scopa::tour_termine(bool js)
{

    cout << "Appuyez sur Entrée pour continuer" << endl;
    getchar();
    if(joueurs[joueurcourant].humain)
    {
        system("CLS");

    }
    if(js) joueursuivant();
}


void Scopa::affiche()
{
    cout <<endl;
    for (int i = 0; i<joueurs.size(); i++)
    {
        if (joueurs[i].humain)
            cout << "Joueur " ;
        else cout << "Ordinateur ";
        cout << joueurs[i].id << " -> Nombre de cartes restantes : " << joueurs[i].main.getSize() << " Score : " << joueurs[i].score <<endl;

    }
    cout << "Sens : " << sens <<endl;
    if (tapis.getSize()!=0)
    {
        cout << "Le tapis est: ";
        tapis.affiche();
        cout<<endl;
    }

}

void Scopa::jouer()
{
    cout << "Début partie  " << nummanche << endl;
    int cmpt=0;
    while (cmpt==0)
    {
        affiche();
        if(joueurs[0].main.getSize()==0 && joueurs[1].main.getSize()==0)
        {
            for (int i=0; i<joueurs.size(); i++)
            {
                pioche.piocher(&joueurs[i].main,3);
            }
            cout<<endl<<endl;
            cout << "Redistribution de cartes" << endl;
        }

        else
        {
            if (joueurs[joueurcourant].humain)
            {
                cout<<"Joueur "<< joueurs[joueurcourant].id << " c'est à vous de jouer. Appuyez sur Entrée lorsque vous êtes prêt";
                getchar();
            }
            else
            {
                cout<<"C'est à l'ordinateur "<< joueurs[joueurcourant].id << " de jouer. Appuyez sur Entrée pour continuer.";
                getchar();
            }

            cout<<endl<<endl;
            joueurs[joueurcourant].affiche(true);
            if (joueurs[joueurcourant].humain)
            {
                cout<<endl<<endl;
                cout<< "Choisissez la carte à jouer puis appuyez sur Entrée."<<endl;
                string carte;
                getline (cin,carte);
                int n = CarteItal::recherche(carte,&joueurs[joueurcourant].main);
                while(n==-1)
                {

                    cout << "Carte invalide.Choississez une autre carte." <<endl;
                    getline (cin,carte);

                    n = CarteItal::recherche(carte,&joueurs[joueurcourant].main);
                }
                joueurs[joueurcourant].main.placer(n,&tapis);
                cout <<endl;
                match_carte();
                if (tapis.getSize()==0)
                {
                    cout << "Felicitations Scopaaaa, tiens un point c'est cadeau"<< endl;
                    cout<<endl;
                    joueurs[joueurcourant].score++;
                }
                joueurs[joueurcourant].affiche(true);
                affiche();
                tour_termine(true);
            }
            else
            {
                cout<<endl<<endl;
                cout<< "Appuyez sur entree pour que l'ordi se reveille."<<endl;
                getchar();
                joueurs[joueurcourant].main.placer(0,&tapis);
                match_carte();
                if (tapis.getSize()==0)
                {
                    cout << "Felicitations Scopaaaa, tiens un point c'est cadeau"<< endl;
                    cout<<endl;
                    joueurs[joueurcourant].score++;
                }
                cout <<endl;
                joueurs[joueurcourant].affiche(true);
                affiche();
                cout <<endl;
                tour_termine(true);
            }
        }

        if (pioche.getSize()==0 && joueurs[0].main.getSize()==0 && joueurs[1].main.getSize()==0)
        {
            cmpt=1;
        }
    }
int taille=tapis.getSize();
    if(tapis.getSize()!=0)
    {
        for (int i=0; i<taille; i++)
        {
            tapis.placer(i,&joueurs[dernierMangeur].plis);
        }
    }
    system("CLS");
    cout << "Partie terminée"<< endl;
    cout<<endl;
    calcul_score();
    affiche();
    nummanche++;
}
Scopa::~Scopa()
{
    //dtor
}
