#ifndef CARTECLASSIQUE_H
#define CARTECLASSIQUE_H

#include "Carte.h"

class PileDeCartes;
class CarteClassique : public Carte
{
    public:
        CarteClassique(int,std::string,int=0,bool=true);
        virtual void affiche() const;
        static  int recherche(std::string,PileDeCartes *p);
        CarteClassique(const CarteClassique& other);
        virtual ~CarteClassique();
    protected:
    private:
};

#endif // CARTECLASSIQUE_H
