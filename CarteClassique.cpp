#include "CarteClassique.h"
#include <iostream>

using namespace std;
CarteClassique::CarteClassique(int v,std::string c,int g,bool vi):Carte(v,c,g,vi)
{
    //ctor
}

void CarteClassique::affiche() const{
    if (visible){
    if (valeur <11){
        cout<<valeur;
    }else if (valeur==11){
        cout<<"V";
    }else if (valeur==12){cout<<"D";}
    else if (valeur==13) {cout<<"R";}
    cout << couleur<< " ";
    }else cout<< "X" << " ";
}

int CarteClassique::recherche(string carte,PileDeCartes* p){
    for (int i=0;i<(p->getSize());i++){
        if (to_string(p->getValeur(i)) + p->getCouleur(i) == carte){
                return i;
        }else if(p->getValeur(i)==11){
            if ("V" + p->getCouleur(i) == carte)
                return i;
        }else if(p->getValeur(i)==12){
            if ("D" + p->getCouleur(i) == carte)
            return i;
        }else if (p->getValeur(i)==13){
            if ("R" + p->getCouleur(i) == carte)
            return i;
        }
    }return -1;
}

CarteClassique::CarteClassique(const CarteClassique& x):Carte(x){
    //copy ctor
}

CarteClassique::~CarteClassique()
{
    //dtor
}
