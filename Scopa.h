#ifndef SCOPA_H
#define SCOPA_H
#include "JeudeCartes.h"
#include "CarteItal.h"


class Scopa: public JeudeCartes
{
public:
    static int dernierMangeur;
    Scopa(int,int);
    void jouer();
    void affiche_regles();
    virtual ~Scopa();
private:
    PileDeCartes tapis = PileDeCartes("tapis");
    void tour_termine(bool=true);
    void affiche();
    void initialisation ();
    void piocher(int=1);
    int gagne();
    bool troisRois(PileDeCartes &p);
    int gagne_manche();
    void match_carte();
    void max_cartes();
    void max_de();
    void max_septde();
    void max_chaqueCouleur();
    void calcul_score();


};

#endif // SCOPA_H
