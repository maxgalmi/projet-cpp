#include "CarteItal.h"
#include <stdio.h>
#include <vector>
#include <cstdlib>
#include <unistd.h>

using namespace std;

CarteItal::CarteItal(int v,string c,int g,bool vi):Carte(v,c,g,vi)
{
}



void CarteItal::affiche() const
{
    if (visible){
    if (valeur <8){
        cout<<valeur;
    }else if (valeur==8){
        cout<<"D";
    }else if (valeur==9){cout<<"C";}
    else if (valeur==10) {cout<<"R";}
    cout << couleur<< " ";
    }else cout<< "X" << " ";
}

int CarteItal::recherche(string carte,PileDeCartes* p){
    for (int i=0;i<(p->getSize());i++){
        if (to_string(p->getValeur(i)) + p->getCouleur(i) == carte){
                return i;
        }else if(p->getValeur(i)==8){
            if ("D" + p->getCouleur(i) == carte)
                return i;
        }else if(p->getValeur(i)==9){
            if ("C" + p->getCouleur(i) == carte)
            return i;
        }else if (p->getValeur(i)==10){
            if ("R" + p->getCouleur(i) == carte)
            return i;
        }
    }return -1;
}

CarteItal::~CarteItal()
{
    //dtor
}

CarteItal::CarteItal(const CarteItal& x):Carte(x){
    //copy ctor
}
