#ifndef BATAILLE_H
#define BATAILLE_H
#include "JeudeCartes.h"
#include "CarteClassique.h"

class Bataille : public JeudeCartes
{
    public:
        Bataille(int,int);


        void jouer();
        void affiche_regles();
        virtual ~Bataille();
    protected:
    private:
        void initialisation();
        PileDeCartes pioche1 = PileDeCartes("pioche");
        PileDeCartes pioche2 = PileDeCartes("pioche");
        void affiche();
        void tour_termine(bool=true);
        void piocher(int=1);
        int gagne();
        int gagne_manche();
        void ajoutergagnant(int);
};

#endif // BATAILLE_H
