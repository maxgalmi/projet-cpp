#ifndef PILEDECARTES_H
#define PILEDECARTES_H
#include "Carte.h"
#include <vector>
#include <string>

class Carte;
class PileDeCartes
{
    public:
        PileDeCartes(std::string);
        void ajouter (Carte*);
        void supprimer();
        void vider();

        std::string getCouleur(int) const;
        int getValeur(int) const;
        int getGain(int) const;
        int getSize() const;
        void setVisible(int,bool);
        void setVisible();
        void setHidden();
        void melange();
        void placer (int, PileDeCartes *);
        void piocher (PileDeCartes *,int=1);
        void deplacertout(PileDeCartes *);
        virtual void affiche () const;

    private:
        std::vector<Carte*> tab;
        std::string type;
};

#endif // PILEDECARTES_H
