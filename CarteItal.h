#ifndef CARTEITAL_H
#define CARTEITAL_H

#include "Carte.h"

class PileDeCartes;
class CarteItal : public Carte
{
    public:
        static int recherche(std::string,PileDeCartes*);
        CarteItal(int,std::string,int=0,bool=true);
        virtual void affiche()const;
        virtual ~CarteItal();
        CarteItal(const CarteItal& other);
    protected:
    private:
};


#endif // CARTEITAL_H
