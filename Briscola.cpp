#include "Briscola.h"
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
using namespace std;

Briscola::Briscola(int j, int o):JeudeCartes(j,o)
{
    if(j+o<2||j+o>5) throw string("2 à 5 joueurs recquis");
    sens = "antihoraire";
    cout << "Yep"<<endl;
    initialisation();

}

void Briscola::initialisation()
{

    melange();
    joueurcourant=0;
    while(pioche.getSize()!=0)
        pioche.supprimer();
    while(tapis.getSize()!=0)
        tapis.supprimer();
    for(int i=0; i<joueurs.size(); i++)
    {
        while(joueurs[i].main.getSize()!=0)
            joueurs[i].main.supprimer();
    }

    pioche.ajouter(new CarteItal(1,"Ep",11));
    pioche.ajouter(new CarteItal(1,"Co",11));
    pioche.ajouter(new CarteItal(1,"Ba",11));
    pioche.ajouter(new CarteItal(1,"De",11));
    pioche.ajouter(new CarteItal(2,"Ep"));
    pioche.ajouter(new CarteItal(2,"Co"));
    pioche.ajouter(new CarteItal(2,"Ba"));
    pioche.ajouter(new CarteItal(2,"De"));
    pioche.ajouter(new CarteItal(3,"Ep",10));
    pioche.ajouter(new CarteItal(3,"Co",10));
    pioche.ajouter(new CarteItal(3,"Ba",10));
    pioche.ajouter(new CarteItal(3,"De",10));
    for(int i = 4; i<8; i++)
    {
        pioche.ajouter(new CarteItal(i,"Ep"));
        pioche.ajouter(new CarteItal(i,"Co"));
        pioche.ajouter(new CarteItal(i,"Ba"));
        pioche.ajouter(new CarteItal(i,"De"));
    }
    pioche.ajouter(new CarteItal(8,"Ep",2));
    pioche.ajouter(new CarteItal(8,"Co",2));
    pioche.ajouter(new CarteItal(8,"Ba",2));
    pioche.ajouter(new CarteItal(8,"De",2));
    pioche.ajouter(new CarteItal(9,"Ep",3));
    pioche.ajouter(new CarteItal(9,"Co",3));
    pioche.ajouter(new CarteItal(9,"Ba",3));
    pioche.ajouter(new CarteItal(9,"De",3));
    pioche.ajouter(new CarteItal(10,"Ep",4));
    pioche.ajouter(new CarteItal(10,"Co",4));
    pioche.ajouter(new CarteItal(10,"Ba",4));
    pioche.ajouter(new CarteItal(10,"De",4));



    pioche.melange();
    for (int i=0; i<joueurs.size(); i++)
    {
        pioche.piocher(&joueurs[i].main,3);
    }
    pioche.piocher(&briscopile);
}


void Briscola::affiche_regles()
{
    cout <<endl;
    cout << "Google, c'est genial : " <<endl;
}

void Briscola::afficheBriscola()
{
    cout << "Briscola est : " << briscopile.getValeur(briscopile.getSize()-1)<< ' '<< briscopile.getCouleur(briscopile.getSize()-1) <<endl;
}

int Briscola::gagne_tour(PileDeCartes &p)
{
    string couleurPremiere;
    int tmp=0;
    p.getCouleur(0)= couleurPremiere;
    int valeurCouleur=0;
    int    valeurAtout=0;

    for (int i=0; i<p.getSize(); i++)
    {
        if (p.getCouleur(i)==couleurPremiere && p.getValeur(i)>valeurCouleur){
            valeurCouleur=p.getValeur(i);
        tmp=i;
        }
    }

    for (int i=0; i<p.getSize(); i++)
    {
        if(p.getCouleur(i)==briscopile.getCouleur(briscopile.getSize()-1)&& p.getValeur(i)>valeurAtout){
            valeurAtout=p.getValeur(i);
        tmp=i;
    }
    }
    return tmp;
}


 int Briscola::gagne_manche()
{
    int tmp(0);
    int j;
    for(int i=0; i<joueurs.size(); i++)
    {
        if (joueurs[i].score>tmp)
        {
            tmp=joueurs[i].score;
            j=i;
        }
        return j;
    }
    return -1;
}


int Briscola::gagne()
{
    for(int i=0; i<joueurs.size(); i++)
    {
        if (joueurs[i].score >= 61) return i;
    }
    return -1;
}

void Briscola::piocher(int n)
{
    if (pioche.getSize()<n && briscopile.getSize()==1)
    {
        briscopile.piocher(&pioche);
    }
    else if (pioche.getSize()<n && briscopile.getSize()<n)
    {
        tapis.deplacertout(&pioche);
        briscopile.piocher(&pioche);
        pioche.piocher(&briscopile);
        if(pioche.getSize()==0)
        {
            cout << "Pioche vide. Piochage annulé." << endl;
        }
        else
        {
            if (pioche.getSize()>=n)
            {
                pioche.piocher(&joueurs[joueurcourant].main,n);
            }
            else
            {
                cout << "Pas assez de cartes dans la pioche, piochage de " << tapis.getSize() << " cartes"<<endl;
                pioche.piocher(&joueurs[joueurcourant].main,tapis.getSize());
            }
        }
    }
    else pioche.piocher(&joueurs[joueurcourant].main,n);


}

void Briscola::tour_termine(bool js)
{

    cout << "Appuyez sur Entrée pour continuer" << endl;
    getchar();
    if(joueurs[joueurcourant].humain)
    {
        system("CLS");

    }
    if(js) joueursuivant();
}

void Briscola::affiche()
{
    cout <<endl;
    for (int i = 0; i<joueurs.size(); i++)
    {
        if (joueurs[i].humain)
            cout << "Joueur " ;
        else cout << "Ordinateur ";
        cout << joueurs[i].id << " -> Nombre de cartes restantes : " << joueurs[i].main.getSize() << " Score : " << joueurs[i].score <<endl;

    }
    cout << "Sens : " << sens <<endl;
    afficheBriscola();
    if (tapis.getSize()!=0)
    {
        cout << "Le tapis est: ";
        tapis.affiche();
        cout<<endl;
    }

}

void Briscola::calcul_score()
{
    for (int i=0; i<joueurs.size(); i++)
    {
        for (int j=0; j<joueurs[i].plis.getSize(); j++)
        {
            joueurs[i].score += joueurs[i].plis.getGain(j);
        }

    }
}

void Briscola::jouer()
{
    while (gagne()==-1){
    cout << "Début partie  " << nummanche << endl;
    affiche();
    while (tapis.getSize()!=joueurs.size())
    {
        if (joueurs[joueurcourant].humain)
        {
            cout<<"Joueur "<< joueurs[joueurcourant].id << " c'est à vous de jouer. Appuyez sur Entrée lorsque vous êtes prêt";
            getchar();
        }
        else
        {
            cout<<"C'est à l'ordinateur "<< joueurs[joueurcourant].id << " de jouer. Appuyez sur Entrée pour continuer.";
            getchar();
        }
        affiche();
        cout<<endl<<endl;
        joueurs[joueurcourant].affiche(true);
        if (joueurs[joueurcourant].humain)
        {
            cout<<endl<<endl;
            cout<< "Choisissez la carte à jouer puis appuyez sur Entrée."<<endl;
            string carte;
            getline (cin,carte);

            int n = CarteItal::recherche(carte,&joueurs[joueurcourant].main);
            while(n==-1){

                   cout << "Carte invalide.Choississez une autre carte." <<endl;
                   getline (cin,carte);

                   n = CarteItal::recherche(carte,&joueurs[joueurcourant].main);
               }
            joueurs[joueurcourant].main.placer(n,&tapis);
            cout <<endl;
            joueurs[joueurcourant].affiche(true);
            affiche();
            pioche.piocher(&joueurs[joueurcourant].main);
            cout <<endl;
            tour_termine(true);
        }
        else
        {
            cout<<endl<<endl;
            cout<< "Appuyez sur entree pour que l'ordi se reveille."<<endl;
            getchar();
            joueurs[joueurcourant].main.piocher(&tapis);
            cout <<endl;
            joueurs[joueurcourant].affiche(true);
            affiche();
            pioche.piocher(&joueurs[joueurcourant].main);
            cout <<endl;
            tour_termine(true);
        }
    }
    int t=gagne_tour(tapis);
    tapis.piocher(&joueurs[t].plis,tapis.getSize());
    calcul_score();
    affiche();
    nummanche++;
}
}


Briscola::~Briscola()
{
    //dtor
}
