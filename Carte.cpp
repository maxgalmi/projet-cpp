#include "Carte.h"
#include <iostream>

using namespace std;

Carte::Carte(int v, string c,int g, bool vi):   valeur(v), couleur(c),gain(g),visible(vi)
{
}



string Carte::getCouleur() const{return couleur;}

int Carte::getValeur() const{return valeur;}

int Carte::getGain() const{return gain;}

void Carte::setVisible(bool b){visible = b;}


Carte::~Carte()
{
    //dtor
}

Carte::Carte(const Carte& x):valeur(x.valeur),couleur(x.couleur),visible(x.visible)
{
    //copy ctor
}
