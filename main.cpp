#include <iostream>
#include "Carte.h"
#include "PileDeCartes.h"
#include "Joueur.h"
#include "Bataille.h"
#include "HuitAmericain.h"
#include "Uno.h"
#include "Briscola.h"
#include "Scopa.h"
#include <ctime>


using namespace std;
int Scopa::dernierMangeur = 0;

int main()
{   //Tests
   srand(time(NULL));
    try{

JeudeCartes * jeu;
  int select = 0;
  do{
  cout << "1 - Bataille" << endl
       << "2 - HuitAmericain" << endl
       << "3 - Uno" << endl
       << "4 - Briscola" << endl
       << "5 - Scopa" << endl
       << "0 - Quitter" <<endl
       << "Entrez code du jeu : "<<endl;
    cin >> select;
    }while(select<0||select>5);
     int  j,o,p;
    if(select==0){return 0;}
     cout << "Nombre de joueurs humain: "<<endl;
    cin >> j;
    cout << "Nombre d'ordinateurs : "<<endl;
    cin >> o;

    if(select==2||select==3){
       cout << "Nombre de points necessaires pour gagner : "<<endl;
       cin>>p;
    }
  switch(select){
  case 1 :
    jeu = new Bataille(j,o);
    break;
  case 2:
    jeu = new HuitAmericain(j,o,p);
    break;
  case 3:
    jeu = new Uno(j,o,p);
    break;
  case 4:
    jeu = new Briscola(j,o);
    break;
  case 5:
   jeu = new Scopa(j,o);
    break;
  default :
    return 0;
  }
    jeu->affiche_regles();

    cout<<endl;
    cout<<"Appuyez sur Entrée pour commencer la partie"<<endl;
    getchar();
    getchar();
    jeu->jouer();

    }catch(string const& e){
        cout<<e<<endl;
    }


    return 0;
}
