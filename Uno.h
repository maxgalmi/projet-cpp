#ifndef UNO_H
#define UNO_H
#include "CarteUno.h"
#include "JeudeCartes.h"


class Uno : public JeudeCartes
{
    public:
	Uno(int,int,int=100);
        PileDeCartes pile = PileDeCartes("pile");
        int cartesapiocher=0;
        virtual void affiche_regles();

        virtual void jouer();

        void setCouleur();
        virtual ~Uno();
    protected:
    private:

        int points;
        std::string couleurcourante;
        void initialisation();
        virtual void affiche();
        virtual int gagne();
        virtual int gagne_manche();
        void joueursuivant();
        void piocher(int=1);
        void action();
        void tour_termine(bool=true);
        void calcul_score(int gagnant);
        std::vector<int> coups_possibles(bool=true);
        std::vector<int> coups_possibles_contre();


};

#endif // UNO_H
