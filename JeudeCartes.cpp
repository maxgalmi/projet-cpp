#include "JeudeCartes.h"
#include "PileDeCartes.h"
using namespace std;

JeudeCartes::JeudeCartes(int nbhumains,int nbordi)
{

    for (int i=0;i<nbhumains;i++){
        joueurs.push_back(Joueur(joueurs.size()+1,true));
    }for (int i=0;i<nbordi;i++){
        joueurs.push_back(Joueur(joueurs.size()+1,false));
    }//ctor
}

void JeudeCartes::affiche_gagnant(int n){
    if(n==-1)
    cout << "Match nul" << endl;
    else {if (joueurs[n].humain) cout << "Le joueur " << (joueurs[n].id) << " gagne." << endl;
    else cout << "L'ordinateur " << (joueurs[n].id) << " gagne." << endl;}
}

void JeudeCartes::melange(){

   if(joueurs.size()>1){
    for (int i=0;i<10;i++){
        int n = rand() % joueurs.size();
        int m = rand() % joueurs.size();
        swap(joueurs[n],joueurs[m]);
    }
   }
}


void JeudeCartes::joueursuivant()
{
    if (sens=="horaire")
    {
        if(joueurcourant==(joueurs.size()-1))
            joueurcourant = 0;
        else joueurcourant++;
    }
    else
    {
        if(joueurcourant==0)
            joueurcourant = joueurs.size()-1;
        else joueurcourant--;
    }

}


void JeudeCartes::change_sens(){
    if (sens=="horaire")
        sens="antihoraire";
   else sens = "horaire";
}

JeudeCartes::~JeudeCartes()
{
    //dtor
}
